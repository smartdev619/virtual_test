const async = require("async");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../model/User");
const config = require("../config/config");
const Utils = require("../Utils/response");

module.exports = {
  /**
   * Add User
   */
  addUser: (req, res) => {
    async.auto(
      {
        encryptPassword: callback => {
          bcrypt.hash(req.body.password, 10, function(err, hashpassword) {
            // encrypt password
            if (err) {
              callback(err);
            } else {
              callback(null, hashpassword);
            }
          });
        },
        insertUser: [
          "encryptPassword",
          (data, callback) => {
            // inser user detail
            req.body.password = data.encryptPassword;
            req.body.adminId = req.decoded.id; // adminid (who creates a user)
            User(req.body).save((err, response) => {
              if (err) {
                callback(err);
              } else {
                callback(null, response);
              }
            });
          }
        ]
      },
      (error, result) => {
        if (error) {
          res.status(401).json(Utils.error(error));
        } else {
          res
            .status(401)
            .json(Utils.success(result.insertUser, Utils.ACCOUNT_ADDED));
        }
      }
    );
  },

  /**
   * update user api
   */
  update: (req, res) => {
    console.log(req.params.id);
    async.auto(
      {
        checkUser: callback => {
          // check user exists in db
          User.findOne({ _id: req.params.id, role: { $ne: "admin" } })
            .lean()
            .exec((err, response) => {
              if (err) {
                callback(err);
              } else if (response != null) {
                callback(null, response);
              } else {
                callback(Utils.USER_NOT_FOUND);
              }
            });
        },
        udpateUser: [
          "checkUser",
          (data, callback) => {
            // udapte user record
            User.findOneAndUpdate(
              { _id: req.params.id },
              { $set: req.body },
              { new: true, upsert: false }
            )
              .lean()
              .exec((err, response) => {
                if (err) {
                  callback(err);
                } else {
                  callback(null, response);
                }
              });
          }
        ]
      },
      (error, result) => {
        if (error) {
          res.status(401).json(Utils.error(error));
        } else {
          delete result.udpateUser.password;
          res
            .status(401)
            .json(Utils.success(result.udpateUser, Utils.UPDATE_USER));
        }
      }
    );
  },

  /**
   * delete User
   */

  delete: (req, res) => {
    async.auto(
      {
        checkUser: callback => {
          // check user exists in db
          User.findOne({ _id: req.params.id, role: { $ne: "admin" } })
            .lean()
            .exec((err, response) => {
              if (err) {
                callback(err);
              } else if (response != null) {
                callback(null, response);
              } else {
                callback(Utils.USER_NOT_FOUND);
              }
            });
        },
        deleteUser: [
          "checkUser",
          (data, callback) => {
            // udapte user record
            User.findOneAndRemove({ _id: req.params.id }).exec(
              (err, response) => {
                if (err) {
                  callback(err);
                } else {
                  callback(null, response);
                }
              }
            );
          }
        ]
      },
      (error, result) => {
        if (error) {
          res.status(401).json(Utils.error(error));
        } else {
          res.status(401).json(Utils.success(null, Utils.DELETE_USER));
        }
      }
    );
  }
};
