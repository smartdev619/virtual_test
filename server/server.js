const express = require("express");
const app = express();
const port = process.env.PORT || 5000;
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const routes = require("./routes");
const config = require("./config/config.js");
const router = express.Router();
const url = process.env.MONGODB_URI || "mongodb://localhost:27017/virtual_test";

try {
  // mongoose.set('debug', true);

  mongoose.connect(url, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  });
} catch (error) {
  console.log(error, "err");
}

routes(router);

app.use(bodyParser.json());localhost:5000/api/v1/user/add

app.use(function (req, res, next) {
  if (!req.headers["content-type"]) {

    return res.status(406).send({
      success: false,
      error: true,
      message: "content type not matched!"
    });

  } else {
    if (req.path == "/api/v1/login" || req.path == "/api/v1/register" || req.path == "/api/v1/forgotpassword") {
      next();
    } else {
      var token =
        req.query.token || req.headers["authorization"] || req.body.token;
      if (token) {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function (err, decoded) {
          if (err) {
            console.log(err, 'errrrr')
            return res.status(401).json({
              error: true,
              success: false,
              message: "Session Expired."
            });
          } else {
            // if everything is good, save to request for use in other routes
            req.decoded = decoded;
            next();
          }
        });
      } else {
        // if there is no token
        // return an error
        return res.status(403).send({
          success: false,
          error: true,
          message: "No token provided."
        });
      }
    }
  }
});

app.use("/api/v1", router);

app.listen(port, () => {
  console.log(`Server started at port: ${port}`);
});
